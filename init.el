(require 'package)

(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(require 'evil-leader)
(global-evil-leader-mode)
(evil-leader/set-leader "<SPC>")
(evil-leader/set-key
  "<SPC>" 'execute-extended-command
  "b" 'buffer-menu
  "eb" 'eval-buffer
  "kb" 'kill-buffer
  "ff" 'find-file
  "fs" 'save-buffer
  "qq" 'save-buffers-kill-terminal
  "s" 'ace-jump-char-mode
)

(require 'evil)
(evil-mode 1)

(require 'whitespace)
(setq whitespace-line-column 80)
(setq whitespace-style '(face empty tabs lines-tail trailing))
(global-whitespace-mode t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files '("~/work/work.org"))
 '(package-selected-packages
   '(ace-jump-mode evil-leader company paredit evil geiser-chicken elpher)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
